const { response } = require('express');
const express = require('express');
const { request } = require('http');
const router = express.Router();

const mysqlConnection = require('../database');

//Guardar Usuario
router.post('/gIdol',(req, res)=>{
    const {nombre, edad, anios, ididol} = req.body;
    console.log({nombre, edad, anios, ididol});
    mysqlConnection.query(`INSERT INTO idol(ididol,nombre, edad, anios) VALUES ( ${ididol},"${nombre}", ${edad}, ${anios});`, (error, rows, fields) =>{
        if (!error) {
            res.json({
                msg: "El usuario fue registrado correctamente"
            })
        } else {
            res.json(error)
        }

    } )
});


//Buscar usuarios--- Mostrar usuarios correcto
router.get('/idols', (resquest, response) =>{
    mysqlConnection.query('SELECT * FROM db_idol.idol', (err,rows,fields) => {
        if(!err){
            response.json(rows)
        }else{
            console.log(err);
        }
    });

})

//Actualizar usuario
router.put('/upIdol/:id', (req, res)=> {
    
    const id= req.params.id;
    const nombre=  req.body.nombre;
    const edad= req.body.edad;
    const anios=req.body.anios;
    
    mysqlConnection.query(
        "UPDATE idol SET nombre= ?, edad= ?, anios=? WHERE ididol= ?",
        [nombre, edad, anios, id],
        (err, rows, fields) => {

            if (!err) {
                res.json({
                    msg: "Idol modificado correctamente"
                });
               
            } else {
                res.json(err);
            }
        })
        });



//Eliminar usuarios correcto

router.delete('/delIdol/:id', (req, res) =>{
    const id= req.params.id;
    
    mysqlConnection.query("DELETE FROM idol WHERE `ididol`= ?;",
    [id], (err, rows, fields) => {
        if (!err) {
            res.json({
                msg: "Idol borrado correctamente"
            });
           
        } else {
            res.json(err);
        }
    })
}
);
//Buscar id unico
router.get('/buscarIdol/:id', (req, res) =>{
    const id=req.params.id;

    mysqlConnection.query("SELECT * FROM idol WHERE ididol= ?;",
    [id], (err, rows, fields)=>{
        if(!err){
            res.json(rows)
        }else{
            console.log(err);
        }
    
})
});
module.exports = router;